echo "Top 5 University in Japan :"
awk '{
  if((n < 5) && (/Japan/)){
    print
    ++n
  }
}' "2023 QS World University Rankings.csv"

printf '\n'
echo "Poin 1 Jika diurutkan dari FSR Terendah :"
awk '/Japan/ {print}' "2023 QS World University Rankings.csv" | sort -t, -nk9 | awk '{
										  if((n<5)){
								  		    print
								  		    ++n
										  }
							     		        }'
                                                                                
printf '\n'
echo  "Top 10 University in Japan by ger-rank :"
awk '{
  if((/Japan/)){
    print
    ++n
  }
}' "2023 QS World University Rankings.csv" | sort -t, -nk20 | awk '{
								if((n<10)){
								  print
								  ++n
								}
							      }'

printf '\n'
echo "Universitas paling keren di dunia : "
awk '/Keren/ {print}' "2023 QS World University Rankings.csv"
