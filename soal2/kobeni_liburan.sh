# 0 */10 * * * bash ~/sisop/Soal2/kobeni_liburan.sh
cd ~/sisop/Soal2

n=$(find -name '*kumpulan*' | wc -l)

if [ $n == 0 ]; then
    n=$((n+1))
    mkdir kumpulan_$n
else
    n=$((n+1))
    mkdir kumpulan_$n
fi

nama_folder="kumpulan_$n"

jam="$(date +%H)"

if [ "$jam" == "00" ]; then
	jam=1;
fi

for ((i=1; i<=$jam;i++)) do
    nama_file="perjalanan_$i.jpg"
    wget https://i2.wp.com/blog.tripcetera.com/id/wp-content/uploads/2020/10/pulau-padar.jpg -O "$nama_file"
    mv -f "$nama_file" "$nama_folder"
done

jam_hour=$(date +"%H")
if [ $jam_hour == "00" ]; then
  zip_ke=$(find -name '*devil*' | wc -l)
  zip_ke=$((zip_ke+1))
  for ((i=1; i<=$n; i=i+1))
  do
    zip -rm devil_$zip_ke kumpulan_$i
  done
fi

