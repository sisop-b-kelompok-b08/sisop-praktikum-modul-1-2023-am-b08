# Praktikum 1 Sistem Operasi 
Perkenalkan kami dari kelas ``Sistem Operasi B Kelompok  B08``, dengan Anggota sebagai berikut:

| Nama                      | NRP        |
|---------------------------|------------|
|Dimas Prihady Setyawan     | 5025211184 |
|Yusuf Hasan Nazila         | 5025211225 |
|Ahda Filza Ghaffaru        | 5025211144 |

# Penjelasan soal nomor 1
Untuk nomor 1, kita perlu mengolah suatu pemeringkatan dari sejumlah universitas di dunia berdasarkan file ``2023 QS World University Rankings.csv`` yang telah diberikan. Terdapat 4 permasalahan yang perlu diselesaikan, berikut adalah langkah-langkahnya.

### Langkah Poin 1
Tampilkan Top 5 Universitas di Jepang. Dikarenakan kita akan membaca file csv per-barisnya, maka perlu digunakan command/peritah *awk* pada script ``university_survey.sh``. Selection criteria yang perlu dilakukan adalah jika n < 5 dan baris mengandung kata kunci "Japan", maka lakukan proses print lalu increment n. 
```
awk '{
    if((n<5) && (/Japan)){
        print
        ++n
    }
}' "2023 QS World University Rankings.csv"
```
Dapat dilihat bahwa variabel n disini berperan sebagai batas dari baris yang akan di print yaitu 5.
```
Top 5 University in Japan :
23,The University of Tokyo,JP,Japan,100,7,99.7,9,91.9,78,73.3,128,10.4,601+,27.8,448,89.5,174,97.8,33,85.3
36,Kyoto University,JP,Japan,98.6,21,98.9,15,94.8,62,54.2,234,14.9,601+,22.1,503,85.5,233,56.9,201,81.4
55,Tokyo Institute of Technology (Tokyo Tech),JP,Japan,74.1,80,93.4,42,81.5,126,65.9,168,36.1,433,37.9,362,55.8,601+,33.6,377,72.5
68,Osaka University,JP,Japan,80.2,68,85.4,61,67.4,205,59.1,206,25,525,14.4,601+,75.2,385,21,567,68.2
79,Tohoku University,JP,Japan,71.8,86,78.1,75,98.6,43,34.2,401,14.1,601+,16.4,595,66.8,493,18.7,601+,64.9
```
### Langkah Poin 2
Tampilkan 5 Universitas di Jepang dengan FSR terendah. Berbeda dengan poin pertama, disini kami lakukan command awk dengan selection criteria universitas yang mengandung kata kunci Japan dahulu. Kemudian, akan dilakukan command *sort* atau pengurutan berdasarkan kolom fsr-score nya yaitu kolom ke-9 dengan delimiter tanda koma (,).
```
awk '/Japan/ {print}' "2023 QS World University Rankings.csv" | sort -t, -nk9
```
Setelah di sort, kami akan melakukan print 5 baris pertama yaitu universitas Jepang dengan fsr terendah. Dengan cara menggunakan awk ditambah pengecekan melalui variabel n.
```
| awk '{
    if((n<5)){
        print
        ++n
    }
  }'
```
```
Poin 1 Jika diurutkan dari FSR Terendah :
891,Ritsumeikan Asia Pacific University,JP,Japan,3.9,501+,10.4,501+,2.5,601+,2.7,601+,99.4,83,99.9,25,5.7,601+,8.1,601+,-
1277,Shibaura Institute of Technology,JP,Japan,3.1,501+,2.6,501+,2.6,601+,7,601+,22,558,9.1,601+,17.2,601+,4.1,601+,-
1244,Kwansei Gakuin University,JP,Japan,3.3,501+,7.4,501+,4.2,601+,3.2,601+,29.7,479,4.1,601+,17,601+,8.6,601+,-     
1042,Doshisha University,JP,Japan,8,501+,15.7,501+,5.8,601+,2.1,601+,20.8,573,4.3,601+,12.3,601+,14.8,601+,-
1249,Meiji University,JP,Japan,6.4,501+,22.1,429,5.8,601+,1.8,601+,10.1,601+,6.2,601+,15.7,601+,16.5,601+,-
```
### Langkah Poin 3
Untuk poin ketiga, kita perlu menampilkan 10 universitas dengan rank (ger-rank) tertinggi di jepang. Maka dari itu, akan dilakukan command awk dengan selection criteria yang mengandung kata kunci "japan" terlebih dahulu, maka print dan increment n. 
```
awk '{
    if((/Japan)){
        print
        ++n
    }
}' "2023 QS World University Rankings.csv"
```
Jika sudah, hasil tersebut akan di sort berdasarkan kolom ke 20 dengan delimiter (,). Dilanjutkan dengan pengambilan 10 baris teratas dengan command awk.
```
| sort -t, -nk20
| awk '{
    if((n<10)){
        print
        ++n
    }
  }'
```
```
Top 10 University in Japan by ger-rank :
23,The University of Tokyo,JP,Japan,100,7,99.7,9,91.9,78,73.3,128,10.4,601+,27.8,448,89.5,174,97.8,33,85.3
197,Keio University,JP,Japan,50.3,154,91.9,48,60.3,258,6.6,601+,10.6,601+,12.4,601+,46,601+,83.7,90,44.1
206,Waseda University,JP,Japan,58.7,121,96.5,27,27.2,558,4.4,601+,28.4,494,35.2,390,63.5,525,72.2,127,42.8
533,Hitotsubashi University,JP,Japan,13.9,501+,84.3,63,30.7,504,2.4,601+,21.2,565,21.9,507,6.4,601+,64.5,169,-
911,Tokyo University of Science,JP,Japan,10.8,501+,46.2,206,11.2,601+,14.5,601+,12.5,601+,3.8,601+,27.7,601+,60.8,188,-
36,Kyoto University,JP,Japan,98.6,21,98.9,15,94.8,62,54.2,234,14.9,601+,22.1,503,85.5,233,56.9,201,81.4
112,Nagoya University,JP,Japan,59.8,117,54.3,153,90.6,84,33.9,403,15.8,601+,20,536,66.5,499,35.1,367,56.3
55,Tokyo Institute of Technology (Tokyo Tech),JP,Japan,74.1,80,93.4,42,81.5,126,65.9,168,36.1,433,37.9,362,55.8,601+,33.6,377,72.5
843,International Christian University,JP,Japan,4.3,501+,12,501+,16.6,601+,1.7,601+,76.4,231,14.8,601+,7,601+,33.5,379,-
135,Kyushu University,JP,Japan,56.6,133,65,113,84.9,110,26.8,466,15.1,601+,20.7,530,66.1,502,22.4,543,53.5

```
### Langkah Poin 4
Untuk poin keempat, kita perlu mencari universitas paling keren di file csv tersebut. Sehingga, kita hanya perlu melakukan command awk biasa dengan selection criteria pada baris yang mengandung kata kunci keren lalu print barisnya.
```
awk '/Keren/ {print}' "2023 QS World University Rankings.csv"
```
```
Universitas paling keren di dunia : 
711,Institut Teknologi Sepuluh Nopember (ITS Surabaya Keren),ID,Indonesia,13,501+,30.3,322,30.7,503,1.8,601+,60,307,2.9,601+,12.2,601+,19.8,595,-
```
# Penjelasan soal nomor 2
### Langkah pertama
```
n=$(find -name '*kumpulan_*' | wc -l)

if [ $n == 0 ]; then
    n=$((n+1))
    mkdir kumpulan_$n
else
    n=$((n+1))
    mkdir kumpulan_$n
fi
```
Untuk langkah pertama, kita membuat dengan folder (n=$(find -name '*kumpulan_*' | wc -l)) adalah untuk mencari apakah ada nama folder "kumpulan" baik ada tambahan kata di depan maupun di belakang. lalu jika tidak ada maka akan membuat dengan awalan 1, lalu jika sudah ada maka akan bertambah sesuai dengan crontab yang ditentukan. 

### Langkah kedua 
```
nama_folder="kumpulan_$n"

jam="$(date +%H)"

for ((i=1; i<=$jam;i++)) do
    if [ "$jam" == "00" ]; then
        i=1;
    fi
    nama_file="foto$i.jpg"
       wget https://i2.wp.com/blog.tripcetera.com/id/wp-content/uploads/2020/10/pulau-padar.jpg -O "$nama_file"
    mv -f "$nama_file" "$nama_folder"
done
```
Langkah kedua, kita beri sesuatu dengan "nama_folder" lalu ditujukan pada folder kumpulan. setelah itu deklarasi jam dengan mengambil date lalu dengan +%H, lalu akan print berulang sebanyak pada jam yang ditentukan. (misal jam 19 maka akan print 19 kali). Jika pada jam 00 maka akan print 1 kali jika tidak menggunakan if maka akan print 0 kali atau tidak print sama sekali. Lalu kita deklarasi sesuatu dengan nama file dan dinamai nama_file yang akan dinamai foto lalu $i diambil dari perulangan foto yang mau didownload. lalu wget untuk mendownlod atau mendapatkan suatu file dengan akhiran format file tsb. lalu -O  digunakan untuk menentukan nama file lokal yang akan digunakan untuk menyimpan hasil unduhan. Lalu unduhan tsb akan ditujukan dan dinamai nama_file. Untuk mv untuk memindahkan dari file ke folder yang kita tuju.


### Langkah Ketiga
```
jam_hour=$(date +"%H")
if [ $jam_hour == "00" ]; then
  zip_ke=$(find -name 'devil' | wc -l)
  zip_ke=$((zip_ke+1))
  for ((i=1; i<=n; i=i+1))
  do
    zip -rm devil_$zip_ke kumpulan_$i
  done
fi
```
Langkah ini akan mencari apakah sudah ada zip atau belum. Jika sudah ada, maka akan bertambah yang akan di zip pada jam 12 malam, yang akan di zip adalah folder yang pada hari itu.(Misal pada hari itu ada 3 folder, maka akan dicek ada zip pada hari sebelumnya atau belum, jika belum maka akan bernama devil_1 dan berisi 3 folder pada hari itu)

### Langkah keempat
```
#!/bin/bash
#0 */10 * * * bash ~/sisop/Soal2/kobeni_liburan.sh
cd ~/sisop/Soal2
```

# Penjelasan soal nomor 3
Instruksi untuk membuat sistem registrasi dan login. Registrasi akan dibuat dalam script ``louis.sh`` dan setiap pengguna yang berhasil mendaftar akan disimpan ke dalam file ``/users/user.txt``. Sistem login akan dibuat pada script ``retep.sh``.

### Langkah-langkah
- Membuat File ``log.txt`` yang bertujuan untuk mencatat login dan registrasi dengan command sebagai berikut
```
touch log.txt
```
- Membuat folder users untuk menyimpan file ``user.txt`` yang memiliki informasi username dan password

```
mkdir users
touch users/users.txt
```
- Membuat ``louis.sh`` untuk registrasi username dan password yang telah ditentukan yaitu

```
nano louis.sh
```

Setelah berhasil dibuat, username dan password disimpan ke ``users/users.txt`` dan status registrasi terupdate di log.txt


- Meminta username dan password dari user untuk registrasi
Meminta input username dan password dari pengguna
```
read -p "Masukkan username: " username
read -p "Masukkan password: " password
```
- Membuat variabel time untuk dimasukan ke ``log.txt``

```
time=$(date +"%y/%m/%d %H:%M:%S")
```
- Mengecek apakah username sudah pernah dibuat atau tidak, jika iya maka akan mengirim informasi ke ``log.txt`` bahwa user telah ada
```
# Mencari apakah username sudah terdaftar atau belum
if grep -q "^$username:" "users/users.txt"; then
    echo "Username sudah terdaftar."
    echo "$time REGISTER: ERROR User already exists" >> log.txt
```
 namun, jika tidak ada, maka akan berlanjut bikin password sesuai dengan kriteria yang telah ditentukan 

1. Minimal 8 karakter
2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
3. Alphanumeric
4. Tidak boleh sama dengan username 
5. Tidak boleh menggunakan kata chicken atau ernie

khusus untuk ``chicken`` dan ``ernie``, kita menggunakan `tr '[:upper:]' '[:lower:]'` untuk mengubah password menjadi lowercase sehingga saat waktu pengecekan ``chicken`` dan ``ernie``, password yang memiliki variasi campuran lowercase mapun uppercase dari ``chicken`` dan ``ernie`` akan terdeteksi. Hal ini hanya berpengaruh saat pengecekan ``chicken`` dan ``ernie`` sehingga tidak mempengaruhi keaslian password dari input user. Berikut adalah implementasi dalam kode

```
else

    # Menentukan kriteria password yang aman
    length=$(echo -n $password | wc -c)
    has_lower=$(echo -n $password | grep -c '[a-z]')
    has_upper=$(echo -n $password | grep -c '[A-Z]')
    has_digit=$(echo -n $password | grep -c '[0-9]')
    is_alphanumeric=$(echo -n $password | grep -c '^[[:alnum:]]*$')
    not_username=$(echo -n $password | grep -c "^$username$")
    contains_chicken=$(echo -n $password | tr '[:upper:]' '[:lower:]' | grep -c 'chicken')
    contains_ernie=$(echo -n $password | tr '[:upper:]' '[:lower:]' | grep -c 'ernie')

    # Memeriksa apakah password memenuhi kriteria yang ditentukan
    if [ $contains_chicken -eq 1 ] || [ $contains_ernie -eq 1 ]; then
        echo "Password tidak boleh menggunakan kata 'chicken' atau 'ernie'."
    elif [ $not_username -eq 1 ]; then
        echo "Password tidak boleh sama dengan username."
    elif [ $length -lt 8 ]; then
        echo "Password harus minimal 8 karakter."
    elif [ $has_lower -eq 0 ] || [ $has_upper -eq 0 ]; then
        echo "Password harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
    elif [ $is_alphanumeric -eq 0 ] || [ $has_digit -eq 0 ]; then
        echo "Password harus alphanumeric"
```
- Setelah password berhasil dibuat, maka username dan password akan dimasukan ke `users/users.txt` dan mengirim informasi ke `log.txt` bahwa Registrasi telah berhasil
```
    else
        # Menambahkan username dan password ke dalam file /users/users.txt
        echo "$username:$password" >> users/users.txt
        echo "$time REGISTER: INFO User $username registered successfully" >> log.txt
        echo "Pendaftaran user berhasil!"
    fi
fi
``` 
Setelah file ``louis.sh`` untuk registrasi dibuat, buatlah file ``retep.sh`` untuk login berdasarkan username dan password yang telah dibuat dari file ``louis.sh`` serta ditulis di file ``users/users.txt``.

```
nano retep.sh
```
- Meminta untuk memasukan username dan passowrd yang telah terdaftar dari file ``louis.sh`` dan disetor di ``users/users.txt``
```
# Meminta input username dan password dari pengguna
read -p "Masukkan username: " username
read -p "Masukkan password: " password
```
- Mencari username dan password yang tersimpan dari file ``users/users.txt``
```
# Mencari username dan password pada file /users/users.txt
result=$(grep "^$username:" users/users.txt)
```
- Menentukan waktu saat pencarian username dan password
```
# Menentukan waktu
time=$(date +"%y/%m/%d %H:%M:%S")
```
- Mencari kecocokan antar username serta password dan untuk tiap kondisi status login dimasukan ke log.txt
1. User berhasil masuk
2. User tidak berhasil masuk karena salah password
3. User tidak berhasil masuk karena username tidak ditemukan
```
# Memeriksa apakah username ditemukan dan password cocok
if [ -n "$result" ]; then
    stored_password=$(echo $result | cut -d ':' -f 2 | tr -d ' ')
    if [ "$password" = "$stored_password" ]; then
        echo "Selamat datang, $username!"
        echo "$time LOGIN: INFO User $username logged in" >> log.txt
    else
        echo "Password yang dimasukkan salah."
        echo "$time LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    fi
else
    echo "Login gagal: username $username tidak ditemukan."
    echo "$time LOGIN: ERROR Username $username not found" >> log.txt
fi
```
# Penjelasan Soal Nomor 4
Untuk nomor 4, kita perlu melakukan proses backup file log sistem kita dengan syarat log sistem harus dienkripsi terlebih dahulu melalui sistem *cipher*. Cipher disini berarti setiap alfabet pada log sistem akan diubah dengan cara ditambah jam dijalankannya script ``log_encrypt.sh``.

### Langkah Enkripsi
Untuk proses enkripsi system log, pertama-tama kita perlu mengambil jam dan tanggal dijalankannya script untuk disimpan didalam variabel filename sebagai nama file backup nantinya dalam bentuk text file.
```
time_date=$(date +"%H:%M %d:%m:%Y")
filename="$time_date.txt"
```
Kemudian kita akan membaca file log sistem yang berada di ``/var/log/syslog`` menggunakan command *cat* dan masukkan ke variabel syslog.
```
syslog="$(cat /var/log/syslog)"
```
Jika sudah, kita akan menyimpan seluruh alfabet dari a sampai z dan juga kapital A-Z di dua variabel yang berbeda  untuk merubah huruf pada syslog, kedua variabel diulang sekali lagi dikarenakan ada kemungkinan huruf z/Z pada syslog ditambah dengan huruf 23 (26 + 23 = 49).
```
uppercase=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ
abcd=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
```
Lalu, akan dilakukan 2 proses enkripsi file syslog melalui command *tr* atau translate, dengan mengubah setiap hurufnya ditambah dengan jam dijalankannya script.
```
jam=$(date +"%H") #catat jam dilakukannya enkripsi, simpan di variabel
encrypt=$(printf '\n%s' "$syslog" | tr "${uppercase:0:26}" "${uppercase:${jam}:26}")
encrypt_2=$(printf '\n%s' "$encrypt" | tr "${abcd:0:26}" "${abcd:${jam}:26}")
```
Setelah enkripsi, kita akan menambah angka jam yang tadi sudah disimpan di baris atas file enkripsi_2 dengan tujuan untuk membantu proses dekripsi nantinya. Jika sudah, masukan kirim hasil enkripsi (jam) ke file tujuan txt (filename).
```
jam+=$encrypt_2 #tambah jam ke baris atas hasil enkripsi, agar nanti bisa di dekripsi
echo "$jam" > "$filename" 
```
Tambahan: Untuk menjalankan script setiap 2 jam, maka dilakukan proses crontab yang sudah ditambahkan di bagian atas source code, berikut format cronjob nya:
```
#0 */2 * * * bash ~/sisop/Soal4/log_encrypt.sh
```
### Langkah Dekripsi
Untuk proses dekripsi, tiap alfabet pada hasil enkripsi yang sudah disimpan di ``$time_date.txt`` akan dikembalikan sebagaimana semula sehingga log sistem dapat dibaca dengan jelas. File dekripsi disimpan pada ``log_decrypt.sh``

Pada langkah pertama, user akan diminta input nama file enkripsi yang ingin di decrypt. Berikut merupakan contoh format input nama file nya.
```
echo "Input nama file yang ingin di decrypt (hh:mm dd:mm:yyyy): "
19:18 03:03:2023 # input
```
Nantinya, tiap alfabet di file enkripsi akan ditambah dengan variabel penambah yang mana penambah adalah jam dilakukannya enkripsi yang sudah ditambah di baris atas file saat enkripsi (diperoleh menggunakan command awk). Selain itu, format penamaan file dekripsi yaitu "Hasil Decrypt" ditambah jam dan tanggal dijalankannya script.
```
penambah=$(awk 'NR==1{print}' "$nama.txt")
hour=$(date +"%H:%M %d:%m:%Y")
filename="Hasil Decrypt $hour.txt"
```
Lalu, file enkripsi syslog akan diawk seluruh barisnya kecuali baris pertama (jam enkripsi) dan simpan di variabel syslog. 
```
syslog="$(awk '{
  if((n == 0)){
    n++
  }
  else {
    print
    n++
  }
}' "$nama.txt")"
``` 
Untuk proses dekripsinya, kita hanya perlu membalik alfabet menjadi Z-A dan z-a yang diulang 2 kali dan disimpan pada variabel``reverse_uppercase`` dan ``reverse_abc``. Selanjutnya, akan dilakukan 2 proses dekripsi menggunakan command tr dengan tujuan untuk mentranslate seluruh variabel di enkripsi yang akan ditambah dengan jam dilakukannya enkripsi, simpan hasil akhir pada variabel ``decrypt_2``.
```
reverse_uppercase=ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA
decrypt=$(echo "$syslog" | tr "${reverse_uppercase:0:26}" "${reverse_uppercase:${penambah}:26}")
reverse_abc=zyxwvutsrqponmlkjihgfedcbazyxwvutsrqponmlkjihgfedcba 
decrypt_2=$(echo "$decrypt" | tr "${reverse_abc:0:26}" "${reverse_abc:${penambah}:26}")
```
Terakhir, hasil dekripsi akan dikirim ke file tujuan backup dalam bentuk .txt
```
echo "$decrypt_2" > "$filename"
```
Hasil dekripsi dari syslog bisa dilihat melalui file txt yang sudah dibuat.
